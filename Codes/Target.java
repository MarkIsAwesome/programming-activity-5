
public interface Target {
    void receive(Projectile per);
    double getPosition();
    
}