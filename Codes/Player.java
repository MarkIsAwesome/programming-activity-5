public class Player {
    private String name;
    private double position;
    Projectile heldProjectile;

    public Player(String name, double position) {
        this.name = name;
        this.position = position;
    }

    public String getName() {
        return this.name;
    }
    public double getPosition() {
        return this.position;
    }

    public boolean giveProjectile(Projectile proj) {
        if (this.heldProjectile == null) {
            this.heldProjectile = proj;
            return true;
        }
        else {
            return false;
        }
    }

    public boolean toss(Target target) {
        if (this.heldProjectile == null) {
            return false;
        }
        else {
            this.heldProjectile.beTossed(this, target);
            return true;
        }
    }
}

