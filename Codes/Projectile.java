public interface Projectile {
    void beTossed(Player per, Target tar);
}